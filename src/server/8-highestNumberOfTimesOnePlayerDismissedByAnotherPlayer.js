const calculateHighestDismissals = function (_, deliveries) {
  const dismissalCounts = {};

  for (const delivery of deliveries) {
    const dismissedPlayer = delivery['player_dismissed'];

    if (dismissedPlayer) {
      const key = `${dismissedPlayer}-${delivery['bowler']}`;
      dismissalCounts[key] = (dismissalCounts[key] || 0) + 1;
    }
  }
  return findMaxDismissals(dismissalCounts);
};
function findMaxDismissals(dismissalCounts) {
  let maxCount = -Infinity;
  let playerWithMostDismissals = '';
  for (const key in dismissalCounts) {
    const count = dismissalCounts[key];

    if (count > maxCount) {
      maxCount = count;
      playerWithMostDismissals = key;
    }
  }
  const result = {};
  result[playerWithMostDismissals] = maxCount;

  return result;
}
module.exports = calculateHighestDismissals;

