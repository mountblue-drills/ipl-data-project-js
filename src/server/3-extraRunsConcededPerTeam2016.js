const extraRunsConcededPerTeam = function (matches, deliveries) {
  const matchesIn2016 = matches.reduce((acc, match) => {
    if (match.season === '2016') {
      acc.add(match.id);
    }
    return acc;
  }, new Set());
  const extraRuns = {};

  deliveries.forEach((delivery) => {
    if (matchesIn2016.has(delivery.match_id)) {
      extraRuns[delivery.bowling_team] =
        (extraRuns[delivery.bowling_team] || 0) + Number(delivery.extra_runs);
    }
  });

  return extraRuns;
};
module.exports = extraRunsConcededPerTeam;
