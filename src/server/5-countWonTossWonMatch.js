let countWonTossWonAlsoMatches = function (matches) {
  return matches.reduce((acc, current) => {
    if (current.tossWinner === current.winner) {
      const winner = current.tossWinner || 'count';   //If current.tossWinner is undefined, it defaults to the string 'count'.
      if (acc[winner]) {
        acc[winner] += 1;
      } else {
        acc[winner] = 1;
      }
    }
    return acc;
  }, {});
};
module.exports = countWonTossWonAlsoMatches;
