let seasonsAndMatchIds=function (matches) {
    return matches.reduce((result, match) => {
      result[match.id] = match.season;
      return result;
    }, {});
}
let strikeRateOfBatsmanPerSeason = function (matches, deliveries) {
  const seasonsAndMatchIdsMap = seasonsAndMatchIds(matches);
  const result = {};

  deliveries.forEach((current) => {
    let ball = 1;
    const season = seasonsAndMatchIdsMap[current.match_id];

    if (current.wide_runs !== '0') {
      ball = 0;
    }

    if (!result[season]) {
      result[season] = {};
    }

    if (!result[season][current.batsman]) {
      result[season][current.batsman] = { runs: 0, balls: 0, StrickeRate: '0.00' };
    }

    result[season][current.batsman].runs += +current.batsman_runs;
    result[season][current.batsman].balls += ball;

    const totalBalls = result[season][current.batsman].balls;
    const totalRuns = result[season][current.batsman].runs;

    if (totalBalls > 0) {
      result[season][current.batsman].StrickeRate = (
        (totalRuns / totalBalls) * 100
      ).toFixed(2);
    }
  });

  return result;
};

module.exports = strikeRateOfBatsmanPerSeason;

  // let strikeRateOfBatsmanPerSeason = function (matches, deliveries) {
  //   const seasonsAndMatchIdsMap = seasonsAndMatchIds(matches);
  
  //   return deliveries.reduce((acc, current) => {
  //     let ball = 1;
  //     let totalBalls = 0;
  //     let totalRuns = 0;
  //     const season = seasonsAndMatchIdsMap[current.match_id];
  
  //     if (current.wide_runs !== '0') {
  //       ball = 0;
  //     }
  
  //     if (!acc[current.batsman]) {
  //       acc[current.batsman] = {};
  //     }
  
  //     if (!acc[current.batsman][season]) {
  //       acc[current.batsman][season] = { runs: 0, balls: 0, StrickeRate: 0 };
  //     }
  
  //     acc[current.batsman][season].runs += +current.batsman_runs;
  //     acc[current.batsman][season].balls += ball;
  
  //     totalBalls = acc[current.batsman][season].balls;
  //     totalRuns = acc[current.batsman][season].runs;
  
  //     if (totalBalls > 0) {
  //       acc[current.batsman][season].StrickeRate = (
  //         (totalRuns / totalBalls) * 100
  //       ).toFixed(2);
  //     }
  
  //     return acc;
  //   }, {});
  // };

  
  // module.exports = strikeRateOfBatsmanPerSeason;
  