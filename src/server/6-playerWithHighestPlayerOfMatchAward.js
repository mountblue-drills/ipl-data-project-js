const findHighestPlayerOfMatchAwardsBySeason = function (matches) {
  const output = {};

  for (let match of matches) {
    const season = match.season;
    const playerOfTheMatch = match['player_of_match'];

    if (!output[season]) {
      output[season] = {};
    }
    if (!output[season][playerOfTheMatch]) {
      output[season][playerOfTheMatch] = 1;
    } else {
      output[season][playerOfTheMatch]++;
    }
  }
  for (let season in output) {
    let maxAwards = 0;
    let highestPlayer = '';

    for (let player in output[season]) {
      if (output[season][player] > maxAwards) {
        maxAwards = output[season][player];
        highestPlayer = player;
      }
    }
    output[season] = highestPlayer;
  }

  return output;
};

module.exports = findHighestPlayerOfMatchAwardsBySeason;

// const findHighestPlayerOfMatchAwardsBySeason = function (matches) {
//   const output = {};
//   for (let match of matches) {
//     output[match.season] = output[match.season] || {};
//     output[match.season][match['player_of_match']] =
//       (output[match.season][match['player_of_match']] || 0) + 1;
//   }
//   for (let season in output) {
//     output[season] = maxValueKey(output[season]);
//   }
//   return output;
// };

// function maxValueKey(obj) {
//   let maxi = -Infinity;
//   let string = '';
//   for (let key in obj) {
//     if (maxi < obj[key]) {
//       maxi = obj[key];
//       string = key;
//     }
//   }
//   return string;
// }

// module.exports = findHighestPlayerOfMatchAwardsBySeason;
