const csv = require('csv-parser');
const fs = require('fs');

//import
const countMatchesPerYear = require('./src/server/1-matchesPerYear');
const matchesWonPerTeamPerYear = require('./src/server/2-countMatchesWonPerTeamPerYear');
const extraRunConcededPerTeam = require('./src/server/3-extraRunsConcededPerTeam2016');
const top10EconomicalBowler2015 = require('./src/server/4-top10EconomicalBowlers2015');
const countWonTossWonAlsoMatches = require('./src/server/5-countWonTossWonMatch')
const calculateBatsmanStrikeRate = require('./src/server/6-playerWithHighestPlayerOfMatchAward')
const strikeRateOfBatsmanPerSeason = require('./src/server/7-strikeRateOfBatsmanPerSeason')
const calculateHighestDismissals = require('./src/server/8-highestNumberOfTimesOnePlayerDismissedByAnotherPlayer')
const bestEconomyBowlerSuperOver = require('./src/server/9-BowlerBestEconomyInSuperOvers')

//calling
csvReadJsonWrite(countMatchesPerYear, '1-matchesPerYear');
csvReadJsonWrite(matchesWonPerTeamPerYear, '2-countMatchesWonPerTeamPerYear');
csvReadJsonWrite(extraRunConcededPerTeam, '3-extraRunsConcededPerTeam2016');
csvReadJsonWrite(top10EconomicalBowler2015, '4-top10EconomicalBowlers2015');
csvReadJsonWrite(countWonTossWonAlsoMatches, '5-countWonTossWonMatch');
csvReadJsonWrite(calculateBatsmanStrikeRate, '6-PlayerWithHighestPlayerOfMatchAward')
csvReadJsonWrite(strikeRateOfBatsmanPerSeason, '7-strikeRateOfBatsmanPerSeason')
csvReadJsonWrite(calculateHighestDismissals, '8-highestNumberOfTimesOnePlayerDismissedByAnotherPlayer')
csvReadJsonWrite(bestEconomyBowlerSuperOver, '9-BowlerBestEconomyInSuperOvers')

function csvReadJsonWrite(cb, path) {
  const matches = [];
  const deliveries = []; // Declare the deliveries array

  fs.createReadStream('./src/data/matches.csv')
    .pipe(csv({}))
    .on('data', (data) => matches.push(data))
    .on('end', () => {
      fs.createReadStream('./src/data/deliveries.csv')
        .pipe(csv({}))
        .on('data', (data) => deliveries.push(data)) // Push data into the deliveries array
        .on('end', () => {
          fs.writeFile(
            `./src/public/output/${path}.json`,
            JSON.stringify(cb(matches, deliveries), null, 2), // Pass both resultsMatches and deliveries to the callback
            (err) => {
              if (err) throw err;
              console.log('The file has been saved!');
            },
          );
        });
    });
}

