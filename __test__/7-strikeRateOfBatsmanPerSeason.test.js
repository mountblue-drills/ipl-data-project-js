const calculateBatsmanStrikeRate = require('../src/server/7-strikeRateOfBatsmanPerSeason');

const sampleMatches = [
  { id: 1, season: '2015' },
  { id: 2, season: '2015' },
  { id: 3, season: '2015' },
  { id: 4, season: '2016' },
  { id: 5, season: '2016' },
  { id: 6, season: '2016' },
  { id: 7, season: '2016' },
  { id: 8, season: '2016' },
];

const sampleDeliveries = [
  { match_id: 1, batsman: 'Kumar', batsman_runs: '2', wide_runs: '0' },
  { match_id: 1, batsman: 'Umesh', batsman_runs: '4', wide_runs: '0' },
  { match_id: 2, batsman: 'Ishant', batsman_runs: '1', wide_runs: '0' },
  { match_id: 2, batsman: 'Nehra', batsman_runs: '3', wide_runs: '0' },
  { match_id: 3, batsman: 'Starc', batsman_runs: '4', wide_runs: '0' },
  { match_id: 3, batsman: 'Suresh', batsman_runs: '1', wide_runs: '0' },
  { match_id: 4, batsman: 'Ashwin', batsman_runs: '2', wide_runs: '0' },
  { match_id: 4, batsman: 'Jadeja', batsman_runs: '2', wide_runs: '0' },
  { match_id: 5, batsman: 'Thakur', batsman_runs: '3', wide_runs: '0' },
  { match_id: 5, batsman: 'Ishan', batsman_runs: '2', wide_runs: '0' },
  { match_id: 6, batsman: 'Kumar', batsman_runs: '3', wide_runs: '0' },
  { match_id: 6, batsman: 'Shami', batsman_runs: '3', wide_runs: '0' },
  { match_id: 7, batsman: 'Umesh', batsman_runs: '2', wide_runs: '0' },
  { match_id: 7, batsman: 'Ishant', batsman_runs: '1', wide_runs: '0' },
  { match_id: 7, batsman: 'Umesh', batsman_runs: '2', wide_runs: '0' },
  { match_id: 8, batsman: 'Kumar', batsman_runs: '4', wide_runs: '0' },
  { match_id: 8, batsman: 'Shami', batsman_runs: '2', wide_runs: '0' },
  { match_id: 8, batsman: 'Umesh', batsman_runs: '13', wide_runs: '0' },
  { match_id: 8, batsman: 'Ishant', batsman_runs: '4', wide_runs: '0' },
];

console.log(calculateBatsmanStrikeRate(sampleMatches, sampleDeliveries));

test('batsman strikerate per season', () => {
  expect(
    calculateBatsmanStrikeRate(sampleMatches, sampleDeliveries),
  ).toMatchObject({
    2015: {
      Kumar: { runs: 2, ballsFaced: 1, strikeRate: '200.00' },
      Umesh: { runs: 4, ballsFaced: 1, strikeRate: '400.00' },
      Ishant: { runs: 1, ballsFaced: 1, strikeRate: '100.00' },
      Nehra: { runs: 3, ballsFaced: 1, strikeRate: '300.00' },
      Starc: { runs: 4, ballsFaced: 1, strikeRate: '400.00' },
      Suresh: { runs: 1, ballsFaced: 1, strikeRate: '100.00' },
    },
    2016: {
      Ashwin: { runs: 2, ballsFaced: 1, strikeRate: '200.00' },
      Jadeja: { runs: 2, ballsFaced: 1, strikeRate: '200.00' },
      Thakur: { runs: 3, ballsFaced: 1, strikeRate: '300.00' },
      Ishan: { runs: 2, ballsFaced: 1, strikeRate: '200.00' },
      Kumar: { runs: 7, ballsFaced: 2, strikeRate: '350.00' },
      Shami: { runs: 5, ballsFaced: 2, strikeRate: '250.00' },
      Umesh: { runs: 17, ballsFaced: 3, strikeRate: '566.67' },
      Ishant: { runs: 5, ballsFaced: 2, strikeRate: '250.00' },
    },
  });
});
