const { test, expect } = require('@jest/globals');
const countMatchesPerYear = require('../src/server/1-matchesPerYear');

const sample = [
  {
    season: '2001',
  },
  {
    season: '2002',
  },
  {
    season: '2002',
  },
  {
    season: '2004',
  },
  {
    season: '2003',
  },
  {
    season: '2003',
  },
  {
    season: '2004',
  },
  {
    season: '2004',
  },
  {
    season: '2004',
  },
  {
    season: '2007',
  },
  {
    season: '2010',
  },
];

test('calculate matches per year', () => {
  expect(countMatchesPerYear(sample)).toMatchObject({
    2001: 1,
    2002: 2,
    2003: 2,
    2004: 4,
    2007: 1,
    2010: 1,
  });
});
