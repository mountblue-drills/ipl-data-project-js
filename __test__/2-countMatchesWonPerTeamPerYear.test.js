const { test, expect } = require('@jest/globals');
const matchesWonPerTeamPerYear = require('../src/server/2-countMatchesWonPerTeamPerYear');

const sample = [
  {
    season: '2001',
    winner: 'SRH',
  },
  {
    season: '2001',
    winner: 'DC',
  },
  {
    season: '2002',
    winner: 'MI',
  },
  {
    season: '2002',
    winner: 'KKR',
  },
  {
    season: '2002',
    winner: 'CSK',
  },
  {
    season: '2002',
    winner: 'MI',
  },
  {
    season: '2003',
    winner: 'CSK',
  },
  {
    season: '2003',
    winner: 'SRH',
  },
  {
    season: '2004',
    winner: 'MI',
  },
  {
    season: '2004',
    winner: 'MI',
  },
  {
    season: '2004',
    winner: 'KKR',
  },
  {
    season: '2004',
    winner: 'MI',
  },
];

test('calculate matches won per team per year', () => {
  expect(matchesWonPerTeamPerYear(sample)).toMatchObject({
    2001: {
      SRH: 1,
      DC: 1,
    },
    2002: {
      MI: 2,
      KKR: 1,
      CSK: 1,
    },
    2003: {
      CSK: 1,
      SRH: 1,
    },
    2004: {
      MI: 3,
      KKR: 1,
    },
  });
});
