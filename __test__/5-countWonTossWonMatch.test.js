const countWonTossWonAlsoMatches = require('../src/server/5-countWonTossWonMatch');
const sample = [
  { winner: 'Mumbai', tossWinner: 'Mumbai' },
  { winner: 'Kolkata', tossWinner: 'Kolkata' },
  { winner: 'Pune', tossWinner: 'Pune' },
  { winner: 'Goa', tossWinner: 'Goa' },
  { winner: 'Patna', tossWinner: 'Patna' },
  { winner: 'Pune', tossWinner: 'Pune' },
  { winner: 'Pune', tossWinner: 'Patna' },
  { winner: 'Pune', tossWinner: 'Goa' },
  { winner: 'Patna', tossWinner: 'Goa' },
];
console.log(countWonTossWonAlsoMatches(sample));
test('number of times each team won the toss and won the match', () => {
  expect(countWonTossWonAlsoMatches(sample)).toMatchObject({
    Mumbai: 1,
    Pune: 2,
    Goa: 1,
    Patna: 1,
    Kolkata: 1,
  });
});
