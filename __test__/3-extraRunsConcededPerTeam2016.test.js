const extraRunsConcededPerTeam = require('../src/server/3-extraRunsConcededPerTeam2016');

const sampleMatches = [
  { id: 1, season: '2018' },
  { id: 2, season: '2018' },
  { id: 3, season: '2015' },
  { id: 4, season: '2015' },
  { id: 5, season: '2016' },
  { id: 6, season: '2016' },
  { id: 7, season: '2016' },
  { id: 8, season: '2016' },
];

const sampleDeliveries = [
  { match_id: 1, extra_runs: 2, bowling_team: 'Kolkata' },
  { match_id: 1, extra_runs: 2, bowling_team: 'Goa' },
  { match_id: 2, extra_runs: 0, bowling_team: 'Pune' },
  { match_id: 2, extra_runs: 4, bowling_team: 'Patna' },
  { match_id: 3, extra_runs: 3, bowling_team: 'Goa' },
  { match_id: 3, extra_runs: 2, bowling_team: 'Mumbai' },
  { match_id: 4, extra_runs: 0, bowling_team: 'Kolkata' },
  { match_id: 4, extra_runs: 2, bowling_team: 'Pune' },
  { match_id: 5, extra_runs: 6, bowling_team: 'Kolkata' },
  { match_id: 5, extra_runs: 2, bowling_team: 'Pune' },
  { match_id: 6, extra_runs: 10, bowling_team: 'Goa' },
  { match_id: 6, extra_runs: 2, bowling_team: 'Goa' },
  { match_id: 7, extra_runs: 6, bowling_team: 'Goa' },
  { match_id: 7, extra_runs: 2, bowling_team: 'Mumbai' },
  { match_id: 7, extra_runs: 1, bowling_team: 'Goa' },
  { match_id: 8, extra_runs: 2, bowling_team: 'Patna' },
  { match_id: 8, extra_runs: 7, bowling_team: 'Pune' },
  { match_id: 8, extra_runs: 13, bowling_team: 'Patna' },
  { match_id: 8, extra_runs: 2, bowling_team: 'Pune' },
];

test('calculate extra runs per team in 2016', () => {
  expect(
    extraRunsConcededPerTeam(sampleMatches, sampleDeliveries),
  ).toMatchObject({
    Patna: 15,
    Goa: 19,
    Pune: 11,
    Mumbai: 2,
    Kolkata: 6,
  });
});
